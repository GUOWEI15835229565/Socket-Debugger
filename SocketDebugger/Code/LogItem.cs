﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace SocketClient
{
    /// <summary>
    /// 
    /// </summary>
    public class LogItem
    {
        public LogItem(DateTime dt, byte[] bs, StoragePoint sp)
        {
            if (bs == null || bs.Length == 0)
            {
                throw new ArgumentException("bs is null or bs.Length == 0");
            }

            if (sp == null)
            {
                throw new ArgumentNullException("sp");
            }
            this._dT = dt;
            this._bytes = bs;
            this._storagePoint = sp;
        }

        #region DT
        /// <summary>
        /// 
        /// </summary>
        public DateTime DT
        {
            get
            {
                if (_dT == null)
                {
                    _dT = new DateTime();
                }
                return _dT;
            }
        } private DateTime _dT;
        #endregion //DT

        #region Bytes
        /// <summary>
        /// 
        /// </summary>
        public byte[] Bytes
        {
            get
            {
                if (_bytes == null)
                {
                    _bytes = new byte[0];
                }
                return _bytes;
            }
        } private byte[] _bytes;
        #endregion //Bytes

        public StoragePoint StoragePoint
        {
            get { return _storagePoint; }
        } private StoragePoint _storagePoint;
    }
}
